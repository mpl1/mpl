class MyClass {

  def outerClosure = {
    println this.class.name    // outputs MyClass
    println owner.class.name    // outputs MyClass
    println delegate.class.name  //outputs MyClass
    def nestedClosure = {
      println this.class.name    // outputs MyClass
      println owner.class.name    // outputs MyClass$_closure1
      println delegate.class.name  // outputs MyClass$_closure1
    }
    nestedClosure()
  }

  static main(args) {
  
    def closure = new MyClass().outerClosure
closure()
   
  }

  //MyClass myClass = new MyClass()


 
}

